function traCuuDiem() {
  var diemChuan = document.getElementById("txt-diemChuan").value * 1;
  var diemMon1 = document.getElementById("txt-diem1").value * 1;
  var diemMon2 = document.getElementById("txt-diem2").value * 1;
  var diemMon3 = document.getElementById("txt-diem3").value * 1;
  var khuVuc = document.getElementById("txt-khuVuc").value * 1;
  var doiTuong = document.getElementById("txt-doiTuong").value * 1;

  var chonKhuVuc = document.getElementById("txt-khuVuc");
  var khuVuc = chonKhuVuc.options[chonKhuVuc.selectedIndex].text;

  if (khuVuc == "A") {
    khuVuc = 2;
  } else if (khuVuc == "B") {
    khuVuc = 1.5;
  } else if (khuVuc == "C") {
    khuVuc = 0.5;
  } else {
    khuVuc = 0;
  }

  var chonDoiTuong = document.getElementById("txt-doiTuong");
  var doiTuong = chonDoiTuong.options[chonDoiTuong.selectedIndex].text;

  if (doiTuong == "1") {
    doiTuong = 2;
  } else if (doiTuong == "2") {
    doiTuong = 1.5;
  } else if (doiTuong == "3") {
    doiTuong = 0.5;
  } else {
    doiTuong = 0;
  }

  var tichDiemThi = diemMon1 * diemMon2 * diemMon3;
  var diem3Mon = diemMon1 + diemMon2 + diemMon3;
  var tongDiem = diem3Mon + khuVuc + doiTuong;

  if (tichDiemThi == 0) {
    document.getElementById(
      "result"
    ).innerHTML = `<p> </p> <p> RỚT </p> Tổng Điểm : ${tongDiem} `;
  } else if (tongDiem < diemChuan) {
    document.getElementById(
      "result"
    ).innerHTML = `<p> </p> <p> RỚT </p> Tổng Điểm : ${tongDiem} `;
  } else {
    document.getElementById(
      "result"
    ).innerHTML = `<p> </p> <p> Đậu </p> Tổng Điểm : ${tongDiem} `;
  }
}
