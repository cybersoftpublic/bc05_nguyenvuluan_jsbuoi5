function tongTienDien() {
  var user = document.getElementById("txt-user").value;
  var dienTieuThu = document.getElementById("txt-dienTieuThu").value * 1;
  var tienDien = 0;
  var tienDienMoc1 = 50 * 500;
  var tienDienMoc2 = 50 * 500 + 50 * 650;
  var tienDienMoc3 = 50 * 500 + 50 * 650 + 100 * 850;
  var tienDienMoc4 = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100;

  if (dienTieuThu <= 50) {
    tienDien = dienTieuThu * 500;
  } else if (dienTieuThu <= 100) {
    tienDien = tienDienMoc1 + (dienTieuThu - 50) * 650;
  } else if (dienTieuThu <= 200) {
    tienDien = tienDienMoc2 + (dienTieuThu - 100) * 850;
  } else if (dienTieuThu <= 350) {
    tienDien = tienDienMoc3 + (dienTieuThu - 200) * 1100;
  } else {
    tienDien = tienDienMoc4 + (dienTieuThu - 350) * 1300;
  }

  document.getElementById(
    "result"
  ).innerHTML = `Số tiền điện gia đình ${user} sử dụng là: ${tienDien}`;
}
